# SPDX-License-Identifier: GPL-2.0-only
# Copyright 2018-2022 Rob Herring <robh@kernel.org>

my_pwclient() {
	IFS=' '

	echo "pwclient $@" >&2
	if [ -z "$dryrun" ]; then
		for i in $(seq -s' ' 10); do
			pwclient "$@" && return 0 || sleep 10
		done
	fi
	return 1
}

get_ids_for_subject() {
	grep ' "Not A' | grep "$@" | cut -d' ' -f1 | sort -nr | xargs
}

get_patch_subject() {
	(sed -e 's/.* <.*> "\(.*\)"$/\1/' | sed -e 's/^\[.*\] //') <<< "$*"
}

get_patch_msgid() {
	sed -n -e 's/.* <\([^ ]*\)> ".*/\1/p' <<< "$*"
}

get_patch_pw_id() {
	IFS=' '
	(cut -d' ' -f1 | xargs) <<< "$*"
}
